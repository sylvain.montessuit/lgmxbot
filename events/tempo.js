const { Events, Message } = require('discord.js');
const axios = require("axios");

const prefix = '!';

module.exports = {
    name: Events.MessageCreate,
    execute(message) {
        // Check if the message is from a bot to avoid reacting to bot messages
        if (message.author.bot) return;

        // Send the message text to the specified URL via a POST request
        const args = message.content.trim().split(/ +/g);
        const cmd = args[0].slice(prefix.length).toLowerCase();

        if(cmd === 'tempo') {
            args.shift()
            const url = 'https://smash3000.ovh/lgmx/songs/title/' + args.join(' ');

            try {
                axios.get(url).then(resp => {
                    console.log('Server response:', resp.data);
                    if(resp.data.bpm !== undefined){
                        message.reply("Bah c'est " + resp.data.bpm + ' frère');
                    }
                    else{
                        message.reply("Gros, on la joue pas celle là. ou alors t'as mal écrit");
                    }
                }).catch(err => {
                    console.log('Issue, Server response: ' + err.response.statusText);
                })

            } catch (error) {
                console.error('Error sending the message:', error.response.statusText);
            }
        }
        else if (cmd === 'sync'){
            args.shift()
            const url = 'https://smash3000.ovh/lgmx/songs/title/' + args.join(' ');

            try {
                axios.get(url).then(resp => {
                    console.log('Server response:', resp.data);
                    if(resp.data.synchro === true){
                        message.reply("Les LEDs sont synchro avec la musique");
                    }else
                    {
                        message.reply("Les LEDs sont pas synchro avec la musique");
                    }
                }).catch(err => {
                    console.log('Issue, Server response: ' + err.response.statusText);
                })

            } catch (error) {
                console.error('Error sending the message:', error.response.statusText);
            }
        }
        else if(cmd === 'rappel'){
            args.shift()
            let channel = message.channel;
            let lastMessage = channel.lastMessage;
            let delay = args.shift();
            delay.replace('j','d')
            // Now plus delay, delay can be formatted like 3h, 2m, 1s
            let reminderDate = new Date(lastMessage.createdAt.getTime() + delay);

            //send message to the channel after the time elapse
            setTimeout(() => {
                channel.send("Rappel pour " + message.author.username + ": " + lastMessage.content);
            }, reminderDate.getTime() - Date.now());
        }
        else if(cmd === 'help'){
            message.reply("ça va aller tkt")
        }
        else if(cmd === 'ping'){
            message.reply("pong")
        }
        else if(cmd === 'pong'){
            message.reply("smash")
        }
        else if(cmd === 'smash'){
            message.reply("SAVE")
        }
        else if(cmd === 'SAVE'){
            message.reply("amorti")
        }
        else if(cmd === 'SAVE'){
            message.reply("amorti")
        }
        else if(cmd === 'amorti'){
            message.reply("plongeon")
        }
        else if(cmd === 'plongeon'){
            message.reply("lob")
        }
        else if(cmd === 'lob'){
            message.reply("smash")
        }
        else if(cmd === 'feinte'){
            message.reply("1 point pour toi")
        }
        else if(cmd === 'ftq'){
            message.reply("Laisse tomber Benoît, on n'a pas de personnalité")
        }
        else if(cmd === 'benoit'){
            message.reply("Je vous ai ramené un nouveau jeu, vous allez voir ça prend que 3 heures c'est génial !")
        }
        else if(cmd === 'tom'){
            message.reply("J'sais plus si je vous ai dit j'habitais ici avant :")
            message.reply('https://maps.app.goo.gl/fUqmQFBC7rMvmUdPA')
        }
        else if(cmd === 'mael'){
            message.reply("GNIN GNIN GNIN")
        }
        else if(cmd === 'bruno'){
            message.reply("Et bah c'est  bien")
        }
        else if(cmd === 'gorge'){
            message.reply("Ouai ouai ouai")
        }
        else if(cmd === 'pleuh'){
            message.reply("On s'accorde à 444 parce qu'il fait chaud")
        }
        else if(cmd === 'julien'){
            message.reply("ça va être ouffissime !")
        }
        else if(cmd === 'aurele'){
            message.reply("L'alcool est un très bon antalgique !")
        }
        else if(cmd === 'mendes'){
            message.reply("OK OK OK OK")
        }
    },
};