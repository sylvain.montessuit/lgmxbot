const { Events, Message } = require('discord.js');
const axios = require("axios");

const channelName = 'setlists';

module.exports = {
    name: Events.MessageCreate,
    execute(message) {
        if (message.channel.name === channelName) {
            // Check if the message is from a bot to avoid reacting to bot messages
            if (message.author.bot) return;

            // Send the message text to the specified URL via a POST request
            const url = 'https://smash3000.ovh/lgmx/songs/conduite/text';
            const messageText = message.content;
            try {
                axios.post(url, {text : messageText}).then(resp => {
                    console.log('Message sent to the URL:', messageText);
                    console.log('Server response:', resp.data);
                    message.reply('Lien vers la Conduite en PDF : ' + resp.data.conduite + '\r\n \r\n Tout est compris dedans !')
                    message.react('✅')

                }).catch(err => {
                    console.log('Issue, Server response: '+ err.response.statusText);
                })

            } catch (error) {
                console.error('Error sending the message:', error.response.statusText);
            }
        }
    },
};