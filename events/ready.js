const { Events } = require('discord.js');

module.exports = {
    name: Events.ClientReady,
    once: true,
    execute(client) {
        console.log(`Ready & YOLO ! Logged in as ${client.user.tag}`);
    },
};